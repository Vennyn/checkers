#include "Queen.h"

void Queen::makeMove(sf::Vector2i destination)
{
	std::cout << "queen move";
}

Queen::Queen(int row, int column, e_colors color)
{
	sprite_queen.setPosition(row, column);
	queen_color = color;
	
}

void Queen::draw(sf::RenderTarget& obcject, sf::RenderStates states)const
{
	obcject.draw(sprite_queen, states);
}

void Queen::setTexture(sf::Texture& texture_queen, e_colors color)
{
	if (this->queen_color == color)
		sprite_queen.setTexture(texture_queen);
}