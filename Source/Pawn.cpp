#include "Pawn.h"

void Pawn::makeMove(sf::Vector2i destination)
{
	int tmp_x = destination.x % 100;
	int tmp_y = destination.y % 100;
	destination.x = (destination.x - tmp_x)+10;
	destination.y = (destination.y - tmp_y)+10;
	sprite_pawn.setPosition(destination.x, destination.y);
}

Pawn::Pawn(int row, int column, e_colors color)
{
	std::cout << "HIPAWN \n";
	sprite_pawn.setPosition(row, column);
	pawn_color = color;
	
}

void Pawn::setColor(e_colors color)
{
	colorSeter(color);
}

void Pawn::setTexture(sf::Texture& texture_pawn, e_colors color)
{
	if (this->pawn_color == color)
		sprite_pawn.setTexture(texture_pawn);
}

void Pawn::draw(sf::RenderTarget& object, sf::RenderStates states)const
{
	object.draw(sprite_pawn, states);
}

bool Pawn::checkIfAnotherPawn(sf::Vector2i vec2i)
{
	int vec2i_x = vec2i.x;
	int vec2i_y = vec2i.y;

	if (sprite_pawn.getGlobalBounds().contains(vec2i_x, vec2i_y))return true;
	else return false;
}

e_colors Pawn::checkOwner(sf::Vector2i source)
{
	if (sprite_pawn.getGlobalBounds().contains(source.x, source.y)) return this->pawn_color;
}


bool Pawn::killFigure(sf::Vector2i destiantion)
{
	if (sprite_pawn.getGlobalBounds().contains(destiantion.x, destiantion.y)) return true;
	else return false;
}