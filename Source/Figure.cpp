#include "Figure.h"

void Figure::colorSeter(e_colors color)
{
	this->color_of_figure = color;
}

void Figure::spriteSeter(sf::Texture& texture_figure)
{
	this->sprite_figure.setTexture(texture_figure);
}

const e_colors Figure::getColor()
{
	return this->color_of_figure;
}

sf::Sprite Figure::getSprite()
{
	return sprite_figure;
}

void Figure::draw(sf::RenderTarget& object, sf::RenderStates states)const
{
	object.draw(sprite_figure, states);
}

