#include "Game.h"


Game::Game() :
	sf_game_window(sf::VideoMode(800, 800), "Checkers_Miozga"),
	is_game_stil_on(true),
	is_menu_phase(exit),
	is_ening_phase(false),
	is_board_phase(true)
{}


Game::~Game()
{
	std::cout << "cleard after Game";
}


void Game::loadTexture() //Gonna fix it later 
{
	if (!texture_black_pawn.loadFromFile("C:/Users/micha/Source/Repos/checkers_2020/Checkers/Release/img/whitePawn.png"))throw - 10;
	if (!texture_white_pawn.loadFromFile("C:/Users/micha/Source/Repos/checkers_2020/Checkers/Release/img/czerwonePion.png"))throw - 10;
	if (!texture_game_board.loadFromFile("C:/Users/micha/Source/Repos/checkers_2020/Checkers/Release/img/warcaby8_8.png"))throw - 10;
	if (!texture_ending.loadFromFile("C:/Users/micha/Source/Repos/checkers_2020/Checkers/Release/img/Wygrana.png"))throw - 10;
	if (!texture_menu.loadFromFile("C:/Users/micha/Source/Repos/checkers_2020/Checkers/Release/img/menu.png"))throw - 10;
	if (!texture_queen_black.loadFromFile("C:/Users/micha/Source/Repos/checkers_2020/Checkers/Release/img/redQueen.png"))throw - 10;
	if (!texture_queen_white.loadFromFile("C:/Users/micha/Source/Repos/checkers_2020/Checkers/Release/img/whiteQueen.png"))throw - 10;

	//do function to set Textures
	s_menu.setTexture(texture_menu);
	sprite_game_board.setTexture(texture_game_board);
	sprite_ending.setTexture(texture_ending);
	//Temp call
	//mainGameLooop();

}

bool Game::checkCoords(std::pair<sf::Vector2i, sf::Vector2i>)
{

	return 1;



}

void Game::mainGameLooop()
{
	
	//setTextures need to do in dif fun
	fg_manager.setFigures();
	fg_manager.setTexture(texture_white_pawn, WHITE);
	fg_manager.setTexture(texture_queen_white, WHITE_QUEEN);
	fg_manager.setTexture(texture_queen_black, BLACK_QUEEN);
	fg_manager.setTexture(texture_black_pawn, BLACK);

	sf::Vector2i window_center(sf_game_window.getSize() / 2u);
	sf::Mouse::setPosition(window_center, sf_game_window);
	while (is_game_stil_on) //main loop
	{
		while (is_menu_phase) //Menu loop
		{

			while (sf_game_window.pollEvent(event_main_event)) {
				if (event_main_event.type == sf::Event::MouseButtonPressed) {
					is_menu_phase = false;
					is_board_phase = true;

				}

			}
			sf_game_window.clear();
			sf_game_window.draw(s_menu);
			sf_game_window.display();
		}

		while (is_board_phase) // Game loop
		{
			while (sf_game_window.pollEvent(event_main_event))
			{
				if (event_main_event.type == sf::Event::MouseButtonPressed)
				{
				
					soure_vector2i = sf::Mouse::getPosition(sf_game_window);
					//if (soure_vector2i.x < 0 || soure_vector2i.y < 0|| soure_vector2i.x > 800 || soure_vector2i.y > 800)throw -2;
					std::cout<<"\n" << soure_vector2i.x << " " << soure_vector2i.y << "     x,y of source ";
					if (board_checker.checkCoords(soure_vector2i, true) && fg_manager.checkOwner(soure_vector2i) == tour) {
						move_pair_vector.first = soure_vector2i;
						destination_allow = true;
						std::cout << " source ok \n";
					}
					
				}
				if (event_main_event.type == sf::Event::MouseButtonReleased && destination_allow == true)
				{
					destination_vector2i = sf::Mouse::getPosition(sf_game_window);
					std::cout << "\n" << destination_vector2i.x << " " << destination_vector2i.y << "     x,y of destination";
					if (board_checker.checkCoords(destination_vector2i, false) && board_checker.checkIfSameAxes(soure_vector2i, destination_vector2i)) {
						move_pair_vector.second = destination_vector2i;
						move_available = true;
						std::cout << " Dest ok \n";
					}
					else destination_allow = false;

				}

				if (move_available) {
					//if(board_checker.checkCoords(move_pair_vector)) std::cout<<"Make move";// write checkCoords to check if there are players pawns || checking colors || checking if there are existing pawns
					
					fg_manager.moveFigure(soure_vector2i, destination_vector2i);
					destination_allow = false;
					move_available = false;
					changeTour();

				}
				if (event_main_event.type == sf::Event::Closed)
				{
					is_game_stil_on = false;
					sf_game_window.close();

				}
			}
			sf_game_window.clear();
			sf_game_window.draw(sprite_game_board);
			sf_game_window.draw(fg_manager);
			sf_game_window.display();
		}


		while (is_ening_phase) //Ending loop
		{
			while (sf_game_window.pollEvent(event_main_event))
			{
				if (event_main_event.type == sf::Event::MouseButtonPressed) {
					std::cout << "HI";
				}


				if (event_main_event.type == sf::Event::Closed)
				{
					is_game_stil_on = false;
					sf_game_window.close();
					
				}

			}
	
			sf_game_window.clear();
			sf_game_window.draw(sprite_ending);
			sf_game_window.display();
		}

	}

}


void Game::changeTour()
{
	(tour == BLACK) ? tour = WHITE : tour = BLACK;
}