#include "FigureManager.h"


void FigureManager::setFigures()
{
	for (int row = 0; row < 8; ++row)
		for (int column = 0; column < 8; ++column)
		{
			if ((row + column) % 2) {
				if (row < 3) vec_figures.push_back(std::make_unique<Pawn>(column * 100 + 10, row * 100 + 10, BLACK));
				else if (row > 4) vec_figures.push_back(std::make_unique<Pawn>(column * 100 + 10, row * 100 + 10, WHITE));
			}
		}
	//vec_figures.push_back(std::make_unique<Queen>());

}


bool FigureManager::checkIfAnotherFigure(sf::Vector2i vec2i) // return true if square if taken or returns false if square is free
{
	bool free_square = true;
	for (auto& figure : vec_figures)
	{
		if (figure->checkIfAnotherPawn(vec2i))
		{
			free_square = false;
			break;
		}
	}
	if (free_square)return false;
	else return true;

}


void FigureManager::draw(sf::RenderTarget& obcjet, sf::RenderStates states)const
{
	for (auto& sprite_figure : vec_figures)
	{
		obcjet.draw(*sprite_figure, states);
	}
}

void FigureManager::setTexture(sf::Texture& texture_figure, e_colors color)
{
	for (auto& sprite_figure : vec_figures)
	{
		sprite_figure->setTexture(texture_figure, color);

	}
}

void FigureManager::killFig(sf::Vector2i destination)
{
	bool toKill = false;
	auto it_pawn_to_kill = std::find_if (vec_figures.begin(), vec_figures.end(),
		[destination, &toKill](std::shared_ptr<Figure> figure)->bool {
		if (figure->killFigure(destination)) {
			toKill = true;
			return true;
		}
	});

	if (toKill) {
		vec_figures.erase(it_pawn_to_kill);
		//add counter to Pawns to end game
	}
}


void FigureManager::moveFigure(sf::Vector2i source, sf::Vector2i destination)
{
	bool tmp_check_owner = checkOwner(source);

	if (checkIfAnotherFigure(destination))	// Check if blocked
	{
		sf::Vector2i tmp_coords_to_jump;

		//check owner		|| later i should check if queen for now only for pawns
		if (tmp_check_owner == checkOwner(destination))std::cout << "U can not jump over your own piece";
		else
		{
			if (!tmp_check_owner) 
			{
				
				if (destination.x / 100 - source.x / 100 == -1 && destination.y / 100 - source.y / 100 == -1)//if source to destination == NW
				{
					tmp_coords_to_jump.x = destination.x - 100;
					tmp_coords_to_jump.y = destination.y - 100;

					if (!checkIfAnotherFigure(tmp_coords_to_jump)) { 
						makeMove(source, tmp_coords_to_jump);
						killFig(destination);
					}
					else std::cout << "U can not jump with that piece in SW direction becouse there is another piece";
				
				}
				else if (destination.x / 100 - source.x / 100 == 1 && destination.y / 100 - source.y / 100 == -1) //if source to destination == NE
				{
					tmp_coords_to_jump.x = destination.x + 100;
					tmp_coords_to_jump.y = destination.y - 100;
					if (!checkIfAnotherFigure(tmp_coords_to_jump)) { 
						makeMove(source, tmp_coords_to_jump); 
						killFig(destination);
					}
					else std::cout << "U can not jump with that pice in SE direction";
				}
				else std::cout << "U can't do that fr |BLACK FIG|";
			}
			else 
			{
				if (destination.x / 100 - source.x / 100 == -1 && destination.y / 100 - source.y / 100 == 1)//if source to destination == SW
				{
					tmp_coords_to_jump.x = destination.x - 100;
					tmp_coords_to_jump.y = destination.y + 100;
					if (!checkIfAnotherFigure(tmp_coords_to_jump)) { 
						makeMove(source, tmp_coords_to_jump);
						killFig(destination);
					}
					else std::cout << "U can not jump with that piece in NW direction becouse there is another piece";
				}
				else if (destination.x / 100 - source.x / 100 == 1 && destination.y / 100 - source.y / 100 == 1) //if source to destination == SE
				{
					tmp_coords_to_jump.x = destination.x + 100;
					tmp_coords_to_jump.y = destination.y + 100;
					if (!checkIfAnotherFigure(tmp_coords_to_jump))
					{
						makeMove(source, tmp_coords_to_jump);
						killFig(destination);
					}
					else std::cout << "U can not jump with that pice in NE direction becouse there is anoter piece";
				}
				else std::cout << "U can't do that move |WHITE FIG|";
			}
			
		}
	}
	else // the none blocked move, need to optlimalize quantity of code
	{
		//make a move
		if (!tmp_check_owner) {
			if (destination.x / 100 - source.x / 100 == -1 && destination.y / 100 - source.y / 100 == -1)//if source to destination == NW
			{
				makeMove(source, destination);
			}
			else if (destination.x / 100 - source.x / 100 == 1 && destination.y / 100 - source.y / 100 == -1) //if source to destination == NE
			{
				makeMove(source, destination);
			}
			else std::cout << "Error in non blocked 120 l";
		}
		
		else {
			if (destination.x / 100 - source.x / 100 == -1 && destination.y / 100 - source.y / 100 == 1)//if source to destination == SW
			{
				makeMove(source, destination);
			}
			else if (destination.x / 100 - source.x / 100 == 1 && destination.y / 100 - source.y / 100 == 1) //if source to destination == SE
			{
				makeMove(source, destination);
			}
			else std::cout << "Error in 132 l";

		}
	}
	


	
}

void FigureManager::fillRose(sf::Vector2i destination)
{
	rose.NW.first = (destination.x  /100) -1;
	rose.NW.second = (destination.y /100) -1;

	rose.NE.first = (destination.x  /100) -1;
	rose.NE.second = (destination.y /100) +1;

	rose.SW.first = (destination.x  /100) +1;
	rose.SW.second = (destination.y /100) -1;

	rose.SE.first = (destination.x  /100) +1;
	rose.SE.second = (destination.y /100) +1;

}

bool FigureManager::checkOwner(sf::Vector2i source)
{
	e_colors color;
	for (auto& figure: vec_figures)
	{
		color = figure->checkOwner(source);
		if (color == BLACK || color == BLACK_QUEEN) return true;
		else if(color == WHITE || color == WHITE_QUEEN) return false;
	}
}

void FigureManager::makeMove(sf::Vector2i source, sf::Vector2i destination)
{
	for (auto& figure : vec_figures)
	{
		if (figure->checkIfAnotherPawn(source))figure->makeMove(destination); //searching for pawn to move him 
	}
}