#include "BoardChecker.h"



BoardChecker::BoardChecker()
{
}


BoardChecker::~BoardChecker()
{
}

bool BoardChecker::checkCoords(sf::Vector2i vec_of_coords, bool flag_of_dest) // If flag_of_dest is true it means that the cords passed down by function are from the source ot the move. If flag is set to false it means that cords are from destination point
{
	
	if (vec_of_coords.x < 0 || vec_of_coords.y < 0 || vec_of_coords.x >800 || vec_of_coords.y >800)
		{
			if (flag_of_dest) throw - 2;
			else throw - 3;
		}
	else
		{
			// Checks if those coords are playable squares
			// by checking index of square 
			// and mod sum of index and rows 
			// if yes than retun true else return false
			vec_of_coords.x = vec_of_coords.x / 100;
			vec_of_coords.y = vec_of_coords.y / 100;
			if ((vec_of_coords.x + vec_of_coords.y) % 2 ) return true;
			else return false;
		}
}


bool BoardChecker::checkIfSameAxes(sf::Vector2i vec_of_coords_source, sf::Vector2i vec_of_coords_destination)
{
	int tmp_x_source = vec_of_coords_source.x % 100;
	int tmp_y_source = vec_of_coords_source.y % 100;
	int tmp_x_dest = vec_of_coords_destination.x % 100;
	int tmp_y_dest = vec_of_coords_destination.y % 100;

	vec_of_coords_source.x = vec_of_coords_source.x - tmp_x_source;
	vec_of_coords_source.y = vec_of_coords_source.y - tmp_y_source;
	vec_of_coords_destination.x = vec_of_coords_destination.x - tmp_x_dest;
	vec_of_coords_destination.y = vec_of_coords_destination.y - tmp_y_dest;

	if (vec_of_coords_source.x == vec_of_coords_destination.x || vec_of_coords_source.y == vec_of_coords_destination.y)return false;
	else return true;

}
//
//bool BoardChecker::checkOwner(Figure figure_to_check)
//{
//	if (figure_to_check.getColor()==BLACK)return true;
//	else if(figure_to_check.getColor()==WHITE) return false;
//	else throw - 4;
//}