#pragma once
#include "Interface.h"
#include "FigureManager.h"
#include "BoardChecker.h"
////#include "FigureManager.h"
static class Game {
public:
	Game();
	~Game();	
	void loadTexture();
	void mainGameLooop();
	void changeTour();
	bool checkCoords(std::pair<sf::Vector2i, sf::Vector2i>);
//	//void startFunction();
//	//void switchTurns();
//
//
private:
	e_colors tour = BLACK;

	sf::Event event_main_event;
	sf::RenderWindow sf_game_window;
	sf::Texture texture_game_board;
	sf::Texture texture_black_pawn;
	sf::Texture texture_white_pawn;
	sf::Texture texture_menu;
	sf::Texture texture_ending;
	sf::Texture texture_queen_black;
	sf::Texture texture_queen_white;

	sf::Sprite s_menu;
	sf::Sprite sprite_game_board;
	sf::Sprite sprite_ending;

	FigureManager fg_manager;

	bool is_game_stil_on = true;; // need to ini
	bool is_board_phase;
	bool is_menu_phase;
	bool is_ening_phase;
	bool destination_allow;
	bool move_available;

	sf::Vector2i soure_vector2i;
	sf::Vector2i destination_vector2i;
	sf::Vector2i vec_of_coords;

	std::pair<sf::Vector2i, sf::Vector2i> move_pair_vector;

	BoardChecker board_checker;
};
