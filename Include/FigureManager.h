#pragma once
#include "Interface.h"
#include "Figure.h"
#include "Pawn.h"
#include "Queen.h"

class FigureManager:public sf::Drawable
{
public:
	void draw(sf::RenderTarget& obcject, sf::RenderStates states)const override;
	void setFigures();
	void promotionPawnToQueen() {}; // destination of pawn is new start postion for queen || need to delete pawn and add queen
	virtual void setTexture(sf::Texture& texture_figure, e_colors color);
	bool checkOwner(sf::Vector2i source);
	bool checkIfAnotherFigure(sf::Vector2i vec2i);
	void moveFigure(sf::Vector2i source, sf::Vector2i destination);
	void fillRose(sf::Vector2i destination);
	void makeMove(sf::Vector2i source, sf::Vector2i destination);
	void killFig(sf::Vector2i destination);
	FigureManager() {};
	~FigureManager()
	{
		vec_figures.clear();

	}
	struct s_Rose
	{
		/*
			NW			N			NE
				\		|		 /
				 \		|		/
					
			W --	  square		-- E
					
				/		|		\
			   /		|		 \
		
			SW			S			SE

		*/		
		 
		//White
	std::pair<bool, bool> NW; //	(row -1, col -1)
	std::pair<bool, bool> NE; //	(row -1, col +1)

		//Black
	std::pair<bool, bool> SW; //	(row +1, col -1)
	std::pair<bool, bool> SE; //	(row +1, col +1)

	};
	s_Rose rose;

	
private:
	//std::vector<std::unique_ptr<Figure>> vec_figures;
	std::vector<std::shared_ptr<Figure>> vec_figures;

};