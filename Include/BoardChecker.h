#pragma once
#include"Interface.h"
#include "Figure.h"
class BoardChecker
{
public:
	BoardChecker();
	~BoardChecker();
	bool checkCoords(sf::Vector2i vec_of_coords, bool flag_of_dest); // If flag_of_dest is true it means that the cords passed down by function are from the source ot the move. If flag is set to false it means that cords are from destination point
	bool checkIfSameAxes(sf::Vector2i vec_of_coords_source, sf::Vector2i vec_of_coords_destination);
																	 //	bool checkOwner(Figure figure_to_check);

};