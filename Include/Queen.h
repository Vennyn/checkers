#pragma once
#include"Figure.h"

class Queen : public Figure, sf::Drawable
{
public:
	Queen(int row, int column, e_colors color);
	void makeMove(sf::Vector2i destination) override;
	virtual void setTexture(sf::Texture& texture_pawn, e_colors color) override;

private:
	void draw(sf::RenderTarget& object, sf::RenderStates states)const override;
	sf::Sprite sprite_queen;
	e_colors queen_color;
	
};