#pragma once
#include"Interface.h"

class Figure: public sf::Drawable
{
public:
	
	Figure() {}
	virtual void makeMove(sf::Vector2i destination) {};
	virtual void setTexture(sf::Texture& texture_figure, e_colors color) = 0;
	virtual bool checkIfAnotherPawn(sf::Vector2i vec2i)=0;
	virtual e_colors checkOwner(sf::Vector2i source) = 0;
	virtual bool killFigure(sf::Vector2i destination) = 0;
	void colorSeter(e_colors color);
	void spriteSeter(sf::Texture& texture_figure);
	void draw(sf::RenderTarget& object, sf::RenderStates states)const override;
	
	const e_colors getColor();
	sf::Sprite getSprite();

private:
	e_colors color_of_figure;
	sf::Sprite sprite_figure;
	
};