#pragma once
#include "Figure.h"
#include "Interface.h"

class Pawn : public Figure, sf::Drawable
{
public:
	Pawn(int row, int column, e_colors color);
	void makeMove(sf::Vector2i destination) override;
	void setColor(e_colors color);
	bool checkIfAnotherPawn(sf::Vector2i vec2i)override;
	e_colors checkOwner(sf::Vector2i source)override;
	virtual void setTexture(sf::Texture& texture_pawn, e_colors color) override;
	bool killFigure(sf::Vector2i destination)override;
private:
	void draw(sf::RenderTarget& object, sf::RenderStates states)const override;
	sf::Sprite sprite_pawn;
	e_colors pawn_color;
};

